from django.contrib import admin

from .models import *

admin.site.register(Property)
admin.site.register(Client)
admin.site.register(Tenant)
admin.site.register(Tenancy)
