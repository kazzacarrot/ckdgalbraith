from django.conf.urls import url
from . import views

app_name = "reports"
urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^occupation$', views.occupation, name="occupation"),
    url(r'^annual$', views.annual, name="annual"),
]
