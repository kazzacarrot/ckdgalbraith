# coding: latin-1
from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Client(models.Model):
    # basic contact information of the client/owner of the property
    # Don't want to lose any starting "0"s
    phone_number = models.CharField(max_length=11)
    full_name = models.CharField(max_length=50)
    email_address = models.CharField(max_length=200)
    address_line1 = models.CharField(max_length=200)
    address_line2 = models.CharField(max_length=200)
    post_code = models.CharField(max_length=10)

    def __str__(self):
        return self.full_name


@python_2_unicode_compatible
class Property(models.Model):
    # the property address and linked back to the client
    # if a client leaves, they probably don't want you keeping data about
    # their properties
    owner = models.ForeignKey(Client, on_delete=models.CASCADE)
    address_line1 = models.CharField(max_length=200)
    address_line2 = models.CharField(max_length=200)
    post_code = models.CharField(max_length=10)

    def __str__(self):
        return self.address_line1


@python_2_unicode_compatible
class Tenant(models.Model):
    # the basic details about the tenant
    phone_number = models.CharField(max_length=11)
    full_name = models.CharField(max_length=50)
    email_address = models.CharField(max_length=200)

    def __str__(self):
        return self.full_name


@python_2_unicode_compatible
class Tenancy(models.Model):
    # the tenancy record would link a let property to a tenant record
    # it would hold the basic data about the tenancy - the start date, the
    # monthy amount, the payment period (monthy, quarterly, annually)
    # as CKD is based in Scotland, no need to worry about different currencies.
    # but, just incase we join the Euro, lets abstract the currency...
    currency = "£"
    Property = models.ForeignKey(Property, on_delete=models.CASCADE)
    tenant = models.ForeignKey(Tenant, on_delete=models.CASCADE)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    monthly_amount = models.DecimalField(decimal_places=2, max_digits=12)
    payment_periods = (("mo", "monthly"), ("qu", "quarterly"), ("ye", "yearly"))
    payment_period = models.CharField(
        max_length=2,
        choices=payment_periods,
        default="mo")

    def __str__(self):
        return str(self.start_date)

    def getCostPerMonth(self):
        return Tenancy.currency + str(self.montly_amount)

    def getRentCost(self):
        if (self.payment_period == "mo"):
            return self.getCostPerMonth()
        if (self.payment_period == "qu"):
            return Tenancy.currency + str(self.monthly_amount * 3)
        if (self.payment_period == "ye"):
            return Tenancy.currency + str(self.monthly_amount * 12)

    def getYearlyAmount(self):
        # The yearly amount is the amount of money the owner will make in 12
        # months. It should take into account the start and end dates of the
        # tenancy.
        # Has the tenancy already started?
        today = timezone.now()
        nextYear = timezone.now(today.year+1, today.month, today.day)
        if (self.start_date <= today):
            # Will it end this year?
            try:
                if (self.end_date >= nextYear):  # No.
                    return self.monthly_amount * 12
                else:  # Yes.
                    # how many months until their tenancy finishes.
                    i = monthsLeft(self.end_date, today)
                    return self.monthly_amount * i

            except:  # end is indefinite.
                return self.monthly_amount * 12

        else:  # The tenancy will start this year.
            # Will it end this year too? I.E. Three month short assured tenancy.
            try:
                monthsUntilStart = monthsLeft(self.start_date, today)
                if (self.end_date >= nextYear):  # No.
                    return self.monthly_amount * (12 - monthsUntilStart)
                else:  # Yes.
                    # how many months until their tenancy finishes.
                    i = monthsLeft(self.end_date, today)
                    return self.monthly_amount * (i - monthsUntilStart)

            except:  # end is indefinite.
                return self.monthly_amount * 12


def monthsLeft(then, today):
    m = then.month
    t = today.month
    return (t - m) % 12
