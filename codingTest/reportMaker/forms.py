from django import forms


class occupationForm(forms.Form):

    address_line1 = forms.CharField(max_length=100)
    address_line2 = forms.CharField(max_length=12)
    post_code = forms.CharField(max_length=12)


class annualForm(forms.Form):
    client_name = forms.CharField(max_length=100)
    client_phone_number = forms.CharField(max_length=12)
