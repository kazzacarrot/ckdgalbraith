from django.shortcuts import render
from .models import Client, Property, Tenancy
from forms import annualForm, occupationForm


def occupationReport(address_line1, address_line2, post_code):
    try:
        p = Property.objects.get(address_line1=address_line1,
                                 address_line2=address_line2,
                                 post_code=post_code)
    except:
        return {"error": "Couldn't find that property!"}

    try:
        t = Tenancy.objects.filter(Property=p)[0]
        T = t.tenant
        return {"person": T}
    except:
        return {"error": "This property is Vacant"}


def annualProjection(clientName, clientPhoneNumber):
    try:
        c = Client.objects.get(full_name=clientName,
                               phone_number=clientPhoneNumber
                               )
    except:
        return {"error": "Couldn't find that client "}

    try:
        res = []
        totalAmount = 0
        p = Property.objects.filter(owner=c)
        for i in range(0, len(p)):
            print("looping")
            print(p[i])
            try:
                t = Tenancy.objects.get(Property=p[i])
                x = t.getYearlyAmount(thisYear=True)
                totalAmount += x
            except:
                x = 0

            res.append({"address": p[i].address_line1, "yearlyAmount": x})

        return {"properties": res, "totalAmount": totalAmount}

    except:
        return {"error": "This client hasn't got any properties"}


def index(request):
    form1 = occupationForm()
    form2 = annualForm()
    return render(request, "reportMaker/index.html",
                  {"form1": form1,
                   "form2": form2})


def occupation(request):
    if request.method == "GET":
        form = occupationForm()
    else:
        form = occupationForm(request.POST)
        if form.is_valid():
            address_line1 = form.cleaned_data['address_line1']
            address_line2 = form.cleaned_data['address_line2']
            post_code = form.cleaned_data['post_code']
            result = occupationReport(address_line1, address_line2, post_code)
        else:
            result = {"error": "There was a problem with the form."}

        return render(request, "reportMaker/occupation.html", {
            "form": form, "result": result})

    return render(request, "reportMaker/occupation.html", {"form": form})


def annual(request):
    if request.method == "GET":
        form = annualForm()
    else:
        form = annualForm(request.POST)
        if form.is_valid():
            clientName = form.cleaned_data['client_name']
            clientPhoneNumber = form.cleaned_data['client_phone_number']
            result = annualProjection(clientName, clientPhoneNumber)
        else:
            result = {"error": "There was a problem with the form"}

        return render(request, "reportMaker/annual.html", {
            "form": form,
            "result": result
        })

    return render(request, "reportMaker/annual.html", {
        "form": form,
    })
