# Annual and occupation reports

## Run with Docker
### To Build
* docker build -t <image_name> .
### To Run
* docker run -t -p 8000:8080 <image_name>
* view it on localhost:8000

## Run without Docker
* python codingTest/manage.py runserver
* view it on localhost:8000



## Improvements
* The yearly amount in the annual report is calculated by multiplying the monthly amount by 12 it doesn't take into account council tax or other expenses.
* The occupation and annual html templates are very similar, their similarities should be extracted into a header template
* Its not very pretty. A CSS file should be configured.